Example of using Storm in combination with RabbitMQ to process a large stream of commands. The example topology runs locally, but the same setup can run no a multi-node Storm cluster.

![Topology](raw/538a8055f0bdffc6efc4606b6333f5d054f824ce/storm.png)