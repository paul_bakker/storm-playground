package playground;

import backtype.storm.Config;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import com.google.common.collect.Maps;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by paul on 08/04/14.
 */
public class CommandAggregationBolt extends BaseBasicBolt{

    final Map<String, Integer> commandCount = new HashMap<String, Integer>();

    @Override
    public final void execute(Tuple tuple, BasicOutputCollector collector) {
        if (TupleHelpers.isTickTuple(tuple)) {
            emitRankings(collector);
        }
        else {
            updateRankingsWithTuple(tuple);
        }
    }

    private void updateRankingsWithTuple(Tuple tuple) {

        String command = tuple.getStringByField("command");

        if(!commandCount.containsKey(command)) {
            commandCount.put(command, 1);
        } else {
            commandCount.put(command, commandCount.get(command) + 1);
        }
    }

    private void emitRankings(BasicOutputCollector collector) {
        collector.emit(new Values(Maps.newHashMap(commandCount)));

        commandCount.clear();
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("commandCount"));
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        Map<String, Object> conf = new HashMap<String, Object>();
        conf.put(Config.TOPOLOGY_TICK_TUPLE_FREQ_SECS, 1);
        return conf;
    }
}
