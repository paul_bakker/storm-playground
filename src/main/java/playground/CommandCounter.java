package playground;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.spout.Scheme;
import backtype.storm.topology.IRichSpout;
import backtype.storm.topology.TopologyBuilder;
import com.rabbitmq.client.ConnectionFactory;
import io.latent.storm.rabbitmq.RabbitMQSpout;
import io.latent.storm.rabbitmq.config.ConnectionConfig;
import io.latent.storm.rabbitmq.config.ConsumerConfig;
import io.latent.storm.rabbitmq.config.ConsumerConfigBuilder;

/**
 * Created by paul on 08/04/14.
 */
public class CommandCounter {
    public static void main(String[] args) throws Exception {

        TopologyBuilder builder = new TopologyBuilder();

        //builder.setSpout("spout", new RandomCommandSpout(), 5);


        Scheme scheme = new CommandScheme();
        IRichSpout spout = new RabbitMQSpout(scheme);

        ConnectionConfig connectionConfig = new ConnectionConfig("localhost", 5672, "guest", "guest", ConnectionFactory.DEFAULT_VHOST, 10); // host, port, username, password, virtualHost, heartBeat
        ConsumerConfig spoutConfig = new ConsumerConfigBuilder().connection(connectionConfig)
                .queue("hello")
                .prefetch(200)
                .requeueOnFail()
                .build();

        builder.setSpout("spout", spout)
                .addConfigurations(spoutConfig.asMap())
                .setMaxSpoutPending(200);

        builder.setBolt("aggregationCounter", new CommandAggregationBolt(), 8).shuffleGrouping("spout");
        builder.setBolt("count", new FinalCommandCounterBolt(), 12).globalGrouping("aggregationCounter");
        builder.setBolt("output", new OutputWriterBolt()).globalGrouping("count");

        Config conf = new Config();
        conf.setDebug(true);


        conf.setMaxTaskParallelism(3);

        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("commandCount", conf, builder.createTopology());

        Thread.sleep(60000);

        cluster.shutdown();

    }
}
