package playground;

import backtype.storm.spout.Scheme;
import backtype.storm.tuple.Fields;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by paul on 08/04/14.
 */
public class CommandScheme implements Scheme {
    @Override
    public List<Object> deserialize(byte[] bytes) {
        String cmd = new String(bytes);
        List<Object> objects = new ArrayList<Object>();
        objects.add(cmd);

        return objects;
    }

    @Override
    public Fields getOutputFields() {
        return new Fields("command");
    }
}
