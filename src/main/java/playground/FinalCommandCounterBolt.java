package playground;

import backtype.storm.Config;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by paul on 08/04/14.
 */
public class FinalCommandCounterBolt extends BaseBasicBolt {
    final Map<String, Integer> commandCount = new HashMap<String, Integer>();

    @Override
    public void execute(Tuple tuple, BasicOutputCollector basicOutputCollector) {
        if (TupleHelpers.isTickTuple(tuple)) {
            String highestCmnd = null;
            int highestNr = 0;

            for (String command : commandCount.keySet()) {
                Integer count = commandCount.get(command);
                if(count > highestNr) {
                     highestCmnd = command;
                     highestNr = count;
                 }
            }

            commandCount.clear();

            basicOutputCollector.emit(new Values(highestCmnd, highestNr));

        } else {
            @SuppressWarnings("unchecked") Map<String, Integer> aggregatedCount = (Map<String, Integer>) tuple.getValueByField("commandCount");

            for (String command : aggregatedCount.keySet()) {
                if(!commandCount.containsKey(command)) {
                    commandCount.put(command, 1);
                } else {
                    commandCount.put(command, commandCount.get(command) + aggregatedCount.get(command));
                }
            }
        }






    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("topCommand", "count"));
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        Map<String, Object> conf = new HashMap<String, Object>();
        conf.put(Config.TOPOLOGY_TICK_TUPLE_FREQ_SECS, 2);
        return conf;
    }


}
