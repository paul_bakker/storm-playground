package playground;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.util.Random;

/**
 * Created by paul on 08/04/14.
 */
public class RabitMQSender  {
    private final static String QUEUE_NAME = "hello";


    public static void main(String[] args) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        Random rand = new Random();

        for(int i = 0; i < 10000; i++) {
            String[] sentences = new String[] {"FORWARD", "LEFT", "RIGHT"};
            String command = sentences[rand.nextInt(sentences.length)];
            channel.basicPublish("", QUEUE_NAME, null, command.getBytes());
            System.out.println(" [x] Sent '" + command + "'");
        }

        channel.close();
        connection.close();
    }
}
