package playground;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Tuple;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * Created by paul on 08/04/14.
 */
public class OutputWriterBolt extends BaseBasicBolt {
    @Override
    public void execute(Tuple tuple, BasicOutputCollector basicOutputCollector) {

        System.out.println("STARTING WRITE");


        String output = tuple.getString(0) + " - " + tuple.getInteger(1) + "\n";

        System.out.println("WRITING: " + output);

        String pathname = "/Users/paul/tmp/commands.txt";
        if(!new File(pathname).exists()) {
            try {
                Files.createFile(Paths.get(pathname));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {

            Files.write(Paths.get(pathname), output.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {

    }
}
